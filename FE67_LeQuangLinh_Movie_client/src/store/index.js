import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import movie from './reducers/movie'
import thunk from 'redux-thunk'
const reducer = combineReducers({
    movie,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
    reducer,
    composeEnhancers(applyMiddleware(thunk))
);
