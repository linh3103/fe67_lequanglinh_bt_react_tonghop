export const actionType = {
    SET_MOVIES: "SET_MOVIES",
    SET_MOVIE_DETAIL: "SET_MOVIE_DETAIL"
}