import { createAction } from '.';
import { request } from '../../api/request';
import {actionType} from './type'

export const fetchMovies = (currentPage)=>{
    return (dispatch) =>{
        request({
            method: 'GET',
            url: "http://movieapi.cyberlearn.vn/api/QuanLyPhim/LayDanhSachPhimPhanTrang",
            params:{
                maNhom: "GP01",
                soTrang: currentPage,
                soPhanTuTrenTrang: "10"
            }
        }).then((res) =>{
            dispatch(createAction(actionType.SET_MOVIES, res.data.content));
        }).catch((err) =>{
            console.log(err)
        })
    }
}

export const getMovieDetail = (maPhim) => {
    return (dispatch) => {
        request({
            method: "GET",
            url: "http://movieapi.cyberlearn.vn/api/QuanLyPhim/LayThongTinPhim",
            params: {
                MaPhim: maPhim
            }
        }).then((res) => {
            dispatch(createAction(actionType.SET_MOVIE_DETAIL, res.data.content));
        }).catch((err) => {
            console.log(err)
        })
    }
}