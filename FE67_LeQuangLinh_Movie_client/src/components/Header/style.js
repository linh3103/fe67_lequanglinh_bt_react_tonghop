import { makeStyles } from "@material-ui/core"

const useStyle = makeStyles(() => {
    return {
        title:{
            flexGrow: 1,
        },
        navLink:{
            color: "#ffffff",   
            marginLeft: 20,
            fontSize: 18,
            opacity: 0.8,
            textDecoration: 'none'
        },
        activeNavLink:{
            opacity: 1
        }
    }
})

export default useStyle