import { AppBar, Toolbar, IconButton, Typography } from '@material-ui/core';
import LocalMoviesIcon from '@material-ui/icons/LocalMovies'
import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import SignIn from '../../views/Signin';
import SignUp from '../../views/Signup';
import useStyle from './style';

const Header = () => {

    // useStyle
    const classes = useStyle();
    return (
        <div>   
                <AppBar position="static">
                    <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu">
                        <LocalMoviesIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        MY MOVIES
                    </Typography>
                    <NavLink className={classes.navLink} activeClassName={classes.activeNavLink} exact to="/">Home</NavLink>
                        
                        <Fragment>     
                            <SignIn/>                       
                            <SignUp/>
                        </Fragment>
                        
                    </Toolbar>
                </AppBar>
            </div>
    );
}

export default Header;