import { Card, CardActionArea, CardMedia, CardContent, Typography, CardActions, Button } from '@material-ui/core';
import React from 'react';
import { NavLink } from 'react-router-dom';

const MovieItem = (props) => {
    return (
            <Card>
                <CardActionArea>    
                    <CardMedia style={{height: 360}}
                    image={props.movie.hinhAnh} 
                    />
                    <CardContent>
                    <Typography gutterBottom variant="h6" component="h3">
                        {props.movie.tenPhim}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        Ngày khởi chiếu: {props.movie.ngayKhoiChieu.substr(0,10)}
                    </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <NavLink to={`/detail/${props.movie.maPhim}`}>
                        <Button size="small" color="primary">
                            Chi tiết
                        </Button>
                    </NavLink>
                </CardActions>
            </Card>
    );
}

export default MovieItem;