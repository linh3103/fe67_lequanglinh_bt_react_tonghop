import { Button, Card, CardMedia, Container, Grid, Typography } from '@material-ui/core';
import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Header from '../../components/Header';
import { getMovieDetail } from '../../store/actions/movie';
import useStyle from './style';
import MovieIcon from '@material-ui/icons/Movie';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import StarsIcon from '@material-ui/icons/Stars';

const Detail = (props) => {

    const dispatch = useDispatch();

    const movieId = props.match.params.id
    const getMovieInfo = useCallback(() => {
        dispatch(getMovieDetail(movieId))
    }, [dispatch, movieId])

    useEffect(() => {
        getMovieInfo()
    }, [getMovieInfo])

    const movie = useSelector((state) => {
        return state.movie.movieDetail
    })

    const classes = useStyle()

    return (
        <div>
            <Header/>
            {
                movie ? 
                (
                    <Container>
                        <Card className={classes.bigCard}>
                            <CardMedia
                                className={classes.cardImg}
                                image={movie.hinhAnh}
                            />
                        </Card>
                        <Grid className={classes.gridContainer} container spacing={2}>
                            <Grid item xs={12} sm={8}>
                                <Typography align="left" variant="h3">
                                    {movie.tenPhim}
                                </Typography>
                                <Typography align="left" variant="h6">
                                    {movie.moTa}
                                </Typography>
                                <Typography  align="left" variant="p" component="p">
                                    Ngày khởi chiếu: {movie.ngayKhoiChieu.substr(0,10)}
                                </Typography>
                                <Typography align="left" variant="p" component="p">
                                    Đánh giá: {movie.danhGia}
                                </Typography>
                                <Grid className={classes.buttons} container spacing={1}>
                                    <Grid item>
                                        <Button
                                            className={classes.buttonItem}
                                            variant="contained"
                                            color="secondary"
                                            startIcon={<MovieIcon />}
                                        >
                                            Xem phim
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button
                                            className={classes.buttonItem}
                                            variant="contained"
                                            color="primary"
                                            startIcon={<PlayCircleOutlineIcon />}
                                        >
                                            Trailer
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button
                                            className={classes.buttonItem}
                                            variant="outlined"
                                            color="default"
                                            startIcon={<StarsIcon />}
                                        >
                                            Đánh giá
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Container>
                ) :
                <h3>Loading...</h3>
            }
        </div>
    );
}

export default Detail;