import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles(() => {
    return{
        bigCard: {
            display: "flex",
            justifyContent: "center",

            backgroundColor: "black"
        },
        cardImg: {
            height: "500px",
            width: "400px",
        },
        gridContainer: {
            padding: "20px 30px"
        },
        buttons:{
            marginTop: "10px"
        },
    }
})

export default useStyle