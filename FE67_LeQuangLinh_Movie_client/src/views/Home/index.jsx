import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {fetchMovies} from '../../store/actions/movie';
import MovieItem from '../../components/MovieItem';
import Header from '../../components/Header'
import { Button, Container, Grid } from '@material-ui/core';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import useStyle from './style';
import { NavLink } from 'react-router-dom';

const Home = (props) => {
    const dispatch = useDispatch();

    var pageId = props.match.params.id

    const getMovies = useCallback(() => {
        dispatch(fetchMovies(pageId))
    }, [dispatch, pageId])

    const movie = useSelector((state) => {
        return state.movie.movieState
    })

    useEffect(() => {
        getMovies()
    }, [getMovies, pageId]);

    const listPage = [];
    const pages = movie && movie.totalPages;
    for(let i = 1; i <= pages; i++){
        listPage.push(i)
    }

    // useStyle
    const classes = useStyle();

    return (
        <div>
            <Header/>  
            <h1>Danh sách phim</h1>
            <Container>
                <Grid container spacing={3}>
                    {
                        movie ? 
                        (
                            movie.items.map((item, index) => {
                            return (
                                <Grid key={index} item xs={12} sm={6}>
                                    <MovieItem movie = {item}/>
                                </Grid>
                            )
                            })
                        ) :
                        null
                    }
                </Grid>
            </Container>
            {
                movie ? 
                (
                    <div className={classes.btnList}>
                        <Button 
                            size="small" 
                            color="primary"
                            variant="outlined"
                            disabled={pageId <= 1 || !pageId}
                            className={classes.btnItem}>
                            <NavLink 
                                to={pageId==="2" ? "/" : `/page/${+pageId - 1}`}>
                                    <ArrowBackIosIcon/>
                            </NavLink>
                        </Button> 
                        {
                            listPage.map((pageNum) =>{
                                return (
                                    <NavLink key={pageNum} to={`/page/${pageNum}`}>
                                        <Button 
                                        size="small" 
                                        color="primary"
                                        variant={pageNum === +pageId ? "contained" : "outlined"}
                                        className={classes.btnItem}>
                                            {pageNum}
                                        </Button>
                                    </NavLink>
                                )
                            })
                        }
                        <Button 
                            size="small" 
                            color="primary"
                            variant="outlined"
                            disabled={pageId >= pages || !pageId}
                            className={classes.btnItem}>
                            <NavLink 
                                to={`/page/${+pageId + 1}`}>
                                    <ArrowForwardIosIcon/>
                            </NavLink>
                        </Button> 
                    </div>
                ) :
                null
            }
        </div>
    );
}

export default Home;