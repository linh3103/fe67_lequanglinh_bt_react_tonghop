import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles((theme) => {
    return {
        btnList: {
            position: "fixed",
            top: "30%",
            right: "5%",

            display: "flex",
            flexDirection: "column",
        },

        btnItem: {
            marginTop: "20px",
        }
    }
})

export default useStyle