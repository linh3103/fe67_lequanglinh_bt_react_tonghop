import React, { useState } from 'react';
import { 
    Dialog, 
    DialogTitle, 
    TextField, 
    Button
}   from '@material-ui/core';
const SignIn = () => {
    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
        return (
            <div>
                <Button color="default" onClick={handleClickOpen}>
                    Đăng nhập
                </Button>
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Đăng nhập</DialogTitle>
                    <form>
                        <div>
                            <TextField name="taiKhoan" label="Tài khoản" fullWidth="true"/>
                            <TextField name="matKhau" label="Mật khẩu" fullWidth="true"/>
                        </div>
                        <div>
                            <Button onClick={handleClose} color="primary">
                                Đóng
                            </Button>
                            <Button onClick={handleClose} type="submit" color="primary">
                                Đăng nhập
                            </Button>
                        </div>
                    </form>
                </Dialog>
            </div>
        );
    }

export default SignIn;