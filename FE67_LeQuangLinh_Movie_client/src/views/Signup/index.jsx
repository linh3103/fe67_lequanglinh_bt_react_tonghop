import React, { useState } from 'react';
import { 
    Dialog, 
    DialogTitle, 
    DialogContent, 
    TextField, 
    DialogActions, 
    Button 
}   from '@material-ui/core';
import { request } from '../../api/request';
const SignUp = (props) => {

    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const [formValue, setFormValue] = useState({
        taiKhoan: "",
        matKhau: "",
        email: "",
        soDt: "",
        maNhom: "GP01",
        hoTen: ""
    })

    const handleChange = (event) =>{
        setFormValue({
            ...formValue,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        request({
            url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangKy",
            method: "POST",
            data: formValue
        }).then((res) => {
            alert(res.data.message)
        }).catch((err) =>{
            console.log(err.response)
        })
    }

    return (
        <div>
            <Button onClick={handleClickOpen}>
                Đăng ký
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Đăng ký tài khoản</DialogTitle>
                <form>
                    <DialogContent>
                        <TextField onChange={handleChange} name="hoTen" label="Họ tên" fullWidth="true"/>
                        <TextField onChange={handleChange} name="taiKhoan" label="Tài khoản" fullWidth="true"/>
                        <TextField onChange={handleChange} name="matKhau" label="Mật khẩu" fullWidth="true"/>
                        <TextField onChange={handleChange} name="email" label="Email" fullWidth="true"/>
                        <TextField onChange={handleChange} name="soDt" label="Số điện thoại" fullWidth="true"/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Hủy
                        </Button>
                        <Button onClick={handleSubmit} color="primary">
                            Đăng ký
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );
}

export default SignUp;