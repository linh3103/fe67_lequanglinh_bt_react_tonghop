import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Detail from './views/Detail';
import SignIn from './views/Signin'
import SignUp from './views/Signup';
import Home from './views/Home'

function App() {
  return (
    <div className="App">
        <BrowserRouter>
          <Switch>
            <Route path="/detail/:id" component={Detail}/>
            <Route path="/signin" component={SignIn}/>
            <Route path="/signup" component={SignUp}/>
            <Route path="/page/:id" component={Home}/>
            <Route path="/" component={Home}/>
          </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
