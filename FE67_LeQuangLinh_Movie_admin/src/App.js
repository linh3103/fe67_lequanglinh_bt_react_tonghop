import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './views/Home';
import SignIn from './views/SignIn';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route path='/signin' component={SignIn}/>
          <Route path='/' component={Home}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
