import { Container, TextField, Button } from '@material-ui/core';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { signIn } from '../../store/actions/auth';

const SignIn = (props) => {
    const [user, setUser] = useState({
        taiKhoan: "",
        matKhau: ""
    })

    const handleChange = (event) =>{
        setUser({
            ...user,
            [event.target.name]: event.target.value
        })
    }

    const dispatch = useDispatch()
    const handleSubmit = (event) => {
        event.preventDefault();
        dispatch(
            signIn(
                user,
                () => {
                    props.history.push('/')
                }
            )
        )
    }

    return (
        <div>
            <div>
                <h1 style={{ textAlign: "center" }}>Đăng Nhập</h1>
                <Container maxWidth="sm">
                    <form onSubmit={handleSubmit}>
                    <div style={{ marginBottom: 30 }}>
                        <TextField
                        name="taiKhoan"
                        fullWidth
                        label="Tài khoản"
                        variant="outlined" 
                        onChange={handleChange}
                        />
                    </div>
                    <div style={{ marginBottom: 30 }}>
                        <TextField
                        name="matKhau"
                        fullWidth
                        label="Mật khẩu"
                        variant="outlined" 
                        onChange={handleChange}
                        />
                    </div>

                    <div>
                        <Button type="submit" variant="contained" color="primary">
                            Đăng Nhập
                        </Button>
                        <Button type="button" variant="contained" color="secondary">
                            Set default user
                        </Button>
                    </div>
                    </form>
                </Container>
            </div>
        </div>
    );
}

export default SignIn;