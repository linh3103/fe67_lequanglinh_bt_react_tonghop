import { Typography } from '@material-ui/core';
import React from 'react';
import Header from '../../components/Header';

const Home = (props) => {
    return (
        <div>
            <Header/>
            <Typography variant="h4" align="center">
                Danh sách người dùng
            </Typography>
        </div>
    );
}

export default Home