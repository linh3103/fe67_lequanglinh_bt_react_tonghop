import {request} from '../../api/request'
import {createAction} from './index'
import {actionType} from './type'
export const signIn = (account, callback) =>{
    return(dispatch) => {
        request({
            url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangNhap",
            method: "POST",
            data: account
        }).then((res) => {
            dispatch(createAction(actionType.SET_USER, res.data));
            callback();
        }).catch((err) => {
            alert(err.response.data)
        })
    }
}