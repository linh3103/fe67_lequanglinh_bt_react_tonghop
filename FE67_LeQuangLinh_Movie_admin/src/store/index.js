import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from "redux-thunk";
import user from './reducers/userReducer'
import admin from './reducers/adminReducer'
const reducer = combineReducers({
    user,
    admin
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
    reducer,
    composeEnhancers(applyMiddleware(thunk))
)
