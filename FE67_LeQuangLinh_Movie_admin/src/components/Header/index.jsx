import { AppBar, Toolbar, IconButton, Typography, Button } from '@material-ui/core';
import LocalMoviesIcon from '@material-ui/icons/LocalMovies'
import React from 'react';
import { NavLink } from 'react-router-dom';
import useStyle from './style';

const Header = (props) => {
    const classes = useStyle()
    return (
        <div>   
                <AppBar position="static">
                    <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu">
                        <LocalMoviesIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        MY MOVIES
                    </Typography>
                    <Button color="default" className={classes.navLink}>Đăng xuất</Button>
                    </Toolbar>
                </AppBar>
            </div>
    );
}

export default Header;